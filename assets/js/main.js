$(function() {
    $('.eu-cookies .agreement').click(function(e) {
		e.preventDefault();

        var date = new Date();
        date.setFullYear(date.getFullYear() + 10);
        document.cookie = 'eu-cookie-agreement=1; path=/; expires=' + date.toGMTString();
        $('.eu-cookies').hide();

        return false;
    });

    $('#nav-icon1').click(function(){
        $(this).toggleClass('open');
        $('.header ul').slideToggle();
    });
/*
    $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: '.grid-item',
        percentPosition: true
    })*/
});