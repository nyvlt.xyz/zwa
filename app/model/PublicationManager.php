<?php
/**
 * Created by PhpStorm.
 * User: Tomáš Nývlt
 * Date: 22.09.15
 * Time: 11:00
 */

namespace App\Model;

use App\Model\Entity;
use Nette;
use Kdyby\Doctrine\EntityManager;


class PublicationManager extends Nette\Object
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EntityRepository
     */
    private $entityRepository;


    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->entityRepository = $entityManager->getRepository(Entity\Publication::getClassName());
    }

    /**
     * @return array
     */
    public function getPublications()
    {
        return $this->entityRepository->findBy([], ['id' => 'ASC']);
    }

    /**
     * @param $id
     * @return null|Entity\Publication
     */
    public function getPublication($id)
    {
        return $this->entityRepository->find($id);
    }

    /**
     * @param Entity\Publication $article
     */
    public function delete(Entity\Publication $article)
    {
        $this->entityManager->remove($article);
        $this->entityManager->flush();
    }
}