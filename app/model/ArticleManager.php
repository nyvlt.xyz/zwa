<?php

namespace App\Model;

use App\Model\Entity;
use Nette;
use Kdyby\Doctrine\EntityManager;


class ArticleManager extends Nette\Object
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EntityRepository
     */
    private $entityRepository;


    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->entityRepository = $entityManager->getRepository(Entity\Article::getClassName());
    }

    /**
     * @return array
     */
    public function getArticles()
    {
        return $this->entityRepository->findBy([], ['id' => 'DESC']);
    }

    /**
     * @return array
     */
    public function getArticlesActive()
    {
        return $this->entityRepository->findBy(['isActive' => '1']);
    }

    /**
     * @param $id
     * @return null|Entity\Article
     */
    public function getArticle($id)
    {
        return $this->entityRepository->find($id);
    }

    /**
     * @param Entity\Article $article
     */
    public function delete(Entity\Article $article)
    {
        $this->entityManager->remove($article);
        $this->entityManager->flush();
    }
}