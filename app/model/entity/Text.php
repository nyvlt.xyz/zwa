<?php
/**
 * Created by PhpStorm.
 * User: Tomáš Nývlt
 * Date: 09.08.15
 * Time: 23:10
 */
namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kdyby;
use Nette;

/**
 * @ORM\Entity
 */
class Text extends Kdyby\Doctrine\Entities\BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $whatIDo;

    /**
     * @ORM\Column(type="text")
     */
    protected $about;

    /**
     * @ORM\Column(type="text")
     */
    protected $contact;

    /**
     * @ORM\Column(type="text")
     */
    protected $tel;

    /**
     * @ORM\Column(type="text")
     */
    protected $email;

    /**
     * @ORM\Column(type="text")
     */
    protected $idNumber;

    /**
     * @ORM\Column(type="text")
     */
    protected $street;

    /**
     * @ORM\Column(type="text")
     */
    protected $town;

    /**
     * @ORM\Column(type="text")
     */
    protected $firstMenu;

    /**
     * @ORM\Column(type="text")
     */
    protected $secondMenu;

    /**
     * @ORM\Column(type="text")
     */
    protected $thirdMenu;

    /**
     * @ORM\Column(type="text")
     */
    protected $fourthMenu;

    /**
     * @ORM\Column(type="text")
     */
    protected $fifthMenu;

    /**
     * @ORM\Column(type="text")
     */
    protected $footerText;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $design;

    /**
     * @return mixed
     */
    public function getWhatIDo()
    {
        return $this->whatIDo;
    }

    /**
     * @param mixed $whatIDo
     */
    public function setWhatIDo($whatIDo)
    {
        $this->whatIDo = $whatIDo;
    }

    /**
     * @return mixed
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * @param mixed $about
     */
    public function setAbout($about)
    {
        $this->about = $about;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * @param mixed $tel
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getIdNumber()
    {
        return $this->idNumber;
    }

    /**
     * @param mixed $idNumber
     */
    public function setIdNumber($idNumber)
    {
        $this->idNumber = $idNumber;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return mixed
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * @param mixed $town
     */
    public function setTown($town)
    {
        $this->town = $town;
    }

    /**
     * @return mixed
     */
    public function getFirstMenu()
    {
        return $this->firstMenu;
    }

    /**
     * @param mixed $firstMenu
     */
    public function setFirstMenu($firstMenu)
    {
        $this->firstMenu = $firstMenu;
    }

    /**
     * @return mixed
     */
    public function getSecondMenu()
    {
        return $this->secondMenu;
    }

    /**
     * @param mixed $secondMenu
     */
    public function setSecondMenu($secondMenu)
    {
        $this->secondMenu = $secondMenu;
    }

    /**
     * @return mixed
     */
    public function getThirdMenu()
    {
        return $this->thirdMenu;
    }

    /**
     * @param mixed $thirdMenu
     */
    public function setThirdMenu($thirdMenu)
    {
        $this->thirdMenu = $thirdMenu;
    }

    /**
     * @return mixed
     */
    public function getFourthMenu()
    {
        return $this->fourthMenu;
    }

    /**
     * @param mixed $fourthMenu
     */
    public function setFourthMenu($fourthMenu)
    {
        $this->fourthMenu = $fourthMenu;
    }

    /**
     * @return mixed
     */
    public function getFifthMenu()
    {
        return $this->fifthMenu;
    }

    /**
     * @param mixed $fifthMenu
     */
    public function setFifthMenu($fifthMenu)
    {
        $this->fifthMenu = $fifthMenu;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getFooterText()
    {
        return $this->footerText;
    }

    /**
     * @param mixed $footerText
     */
    public function setFooterText($footerText)
    {
        $this->footerText = $footerText;
    }

    /**
     * @return mixed
     */
    public function getDesign()
    {
        return $this->design;
    }

    /**
     * @param mixed $design
     */
    public function setDesign($design)
    {
        $this->design = $design;
    }

}