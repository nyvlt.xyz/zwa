<?php
/**
 * Created by PhpStorm.
 * User: Tomáš Nývlt
 * Date: 09.08.15
 * Time: 23:10
 */
namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kdyby;
use Nette;

/**
 * @ORM\Entity
 */
class Publication extends Kdyby\Doctrine\Entities\BaseEntity
{
    use Kdyby\Doctrine\Entities\Attributes\Identifier;

    /**
     * @ORM\Column(type="text")
     */
    protected $content;

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

}