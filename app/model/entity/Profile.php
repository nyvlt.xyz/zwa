<?php
/**
 * Created by PhpStorm.
 * User: Tomáš Nývlt
 * Date: 09.08.15
 * Time: 23:10
 */
namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kdyby;
use Nette;

/**
 * @ORM\Entity
 */
class Profile extends Kdyby\Doctrine\Entities\BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $profile;

    /**
     * @return mixed
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param mixed $profile
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
    }


}