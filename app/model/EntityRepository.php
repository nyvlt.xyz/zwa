<?php

namespace App\Model;

/**
 * Class EntityRepository
 * @package App\Model
 *
 */
class EntityRepository extends \Kdyby\Doctrine\EntityRepository {

    public function findByOr(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        $qb = $this->createQueryBuilder('e')
            ->whereOrCriteria($criteria)
            ->autoJoinOrderBy((array) $orderBy);

        return $qb->getQuery()
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getResult();
    }


    /**
     * @param string $alias
     * @param string $indexBy The index for the from.
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = NULL, $indexBy = NULL)
    {
        $qb = new QueryBuilder($this->getEntityManager());

        if ($alias !== NULL) {
            $qb->select($alias)->from($this->getEntityName(), $alias, $indexBy);
        }

        return $qb;
    }

    /**
     * @param array $criteria
     * @return int
     */
    public function countByOr(array $criteria)
    {
        return $query = $this->createQueryBuilder('e')
            ->whereOrCriteria($criteria)
            ->select('COUNT(e)')
            ->setMaxResults(1)
            ->getQuery()->getSingleScalarResult();
    }

}