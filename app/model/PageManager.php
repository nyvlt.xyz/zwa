<?php

namespace App\Model;

use App\Model\Entity;
use Nette;
use Kdyby\Doctrine\EntityManager;


class PageManager extends Nette\Object
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EntityRepository
     */
    private $entityRepository;


    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->entityRepository = $entityManager->getRepository(Entity\Text::getClassName());
    }

    /**
     * @return array
     * @param $id
     * @return null|Entity\Text
     */
    public function getText($id)
    {
        return $this->entityRepository->find($id);
    }
}