<?php


namespace App\Forms;

use App\Model\Entity\Profile;
use App\Model;
use Kdyby\Doctrine\EntityManager;
use Kdyby\DoctrineForms\EntityFormMapper;
use Nette;
use Nette\Application\UI\Form;


class ProfileForm extends AdminForm
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EntityFormMapper
     */
    private $mapper;

    /**
     * @var Profile
     */
    private $profile;

    /**
     * @param EntityManager $entityManager
     * @param EntityFormMapper $mapper
     */
    public function __construct(EntityManager $entityManager, EntityFormMapper $mapper)
    {
        $this->entityManager = $entityManager;
        $this->mapper = $mapper;
    }


    /**
     * @param Profile $profile
     * @return Form
     */
    public function create(Profile $profile = null)
    {
        $this->profile = $profile;

        $form = parent::create();

        $form->addTextArea('profile', 'Co dělám', NULL, 15)
            ->setAttribute('id', 'wysiwyg');

        $form->addSubmit('send', $this->profile ? 'Upravit' : 'Přidat');
        $form->onSuccess[] = $this->process;

        if ($profile)
            $this->mapper->load($profile, $form);

        return $form;
    }

    /**
     * @param Form $form
     */
    public function process(Form $form)
    {
        $values = $form->getValues();


        if (!$this->profile) {

        } else {
            $this->profile->setProfile($values->profile);

            $this->entityManager->persist($this->profile);
            $this->entityManager->flush($this->profile);
        }
    }
}