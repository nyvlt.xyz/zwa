<?php


namespace App\Forms;

use App\Model;
use App\Model\Entity;
use Kdyby\Doctrine\EntityManager;
use Kdyby\DoctrineForms\EntityFormMapper;
use Nette;
use Nette\Application\UI\Form;
use Nette\Utils\Strings;

class ArticleForm extends AdminForm
{
    const EXTENSION = 'jpg';

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EntityFormMapper
     */
    private $mapper;

    /**
     * @var Entity\Article
     */
    private $article;


    public function __construct(EntityManager $entityManager, EntityFormMapper $mapper)
    {
        $this->entityManager = $entityManager;
        $this->mapper = $mapper;
    }


    /**
     * @param Entity\Article $article
     * @return Form
     */
    public function create(Entity\Article $article = null)
    {
        $form = parent::create();
        $form->addText('title', 'Zadejte název:')
            ->setRequired('Nezadali jste jméno!');

        $form->addUpload('photo', 'Obrázek')
            ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 250 kB', 250 * 1024);

        $form->addTextArea('description', 'Obsah', NULL, 15)
            ->setAttribute('id', 'wysiwyg');

        $form->addText('perex', 'Zadejte perex:')
            ->setRequired('Nezadali jste perex!');

        $form->addCheckbox('isActive', 'Na hlavní stránce');

        /*
        $form->addTextArea('description', 'Popis')
            ->setRequired('Nezadali jste popis')
            ->addCondition(Form::MAX_LENGTH, 'Příliš dlouhý popis', 450);*/

        $form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = $this->process;
        $form->onSuccess[] = $this->uploadPhoto;

        $this->article = $article;

        if ($article)
            $this->mapper->load($article, $form);

        return $form;
    }

    /**
     * @param Form $form
     */
    public function process(Form $form)
    {
        if (!$this->article){
            $this->article = new Entity\Article;
            $this->entityManager->persist($this->article);
        }

        $this->mapper->save($this->article, $form);
        //$this->article->setUrl(Strings::webalize($form->values->name));
        $this->entityManager->flush();
    }

    public function uploadPhoto(Form $form)
    {
        $values = $form->values;

        /** @var $photo Nette\Http\FileUpload */
        $photo = $values->photo;

        if ($photo->isOk()) {
            $photo->move('img/upload/article/' . $this->article->getId() . '.' . self::EXTENSION);
        }
    }
}