<?php


namespace App\Forms;

use App\Model\Entity\Publication;
use App\Model;
use Kdyby\Doctrine\EntityManager;
use Kdyby\DoctrineForms\EntityFormMapper;
use Nette;
use Nette\Application\UI\Form;


class PublicationForm extends AdminForm
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EntityFormMapper
     */
    private $mapper;

    /**
     * @var Publication
     */
    private $publication;

    /**
     * @param EntityManager $entityManager
     * @param EntityFormMapper $mapper
     */
    public function __construct(EntityManager $entityManager, EntityFormMapper $mapper)
    {
        $this->entityManager = $entityManager;
        $this->mapper = $mapper;
    }


    /**
     * @param Publication $publication
     * @return Form
     */
    public function create(Publication $publication = null)
    {
        $this->publication = $publication;

        $form = parent::create();

        $form->addTextArea('content', 'Obsah', NULL, 15)
            ->setAttribute('id', 'wysiwyg');

        $form->addSubmit('send', $this->publication ? 'Upravit' : 'Přidat');
        $form->onSuccess[] = $this->process;

        if ($publication)
            $this->mapper->load($publication, $form);

        return $form;
    }

    /**
     * @param Form $form
     */
    public function process(Form $form)
    {
        $values = $form->getValues();


        if (!$this->publication) {
            $publication = new Publication;
            $publication->setContent($values->content);

            $this->entityManager->persist($publication);
            $this->entityManager->flush($publication);

        }
        else {
            $this->publication->setContent($values->content);

            $this->entityManager->persist($this->publication);
            $this->entityManager->flush($this->publication);
        }
    }
}