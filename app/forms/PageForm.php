<?php


namespace App\Forms;

use App\Model\Entity\Text;
use App\Model;
use Kdyby\Doctrine\EntityManager;
use Kdyby\DoctrineForms\EntityFormMapper;
use Nette;
use Nette\Application\UI\Form;


class PageForm extends AdminForm
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EntityFormMapper
     */
    private $mapper;

    /**
     * @var Text
     */
    private $Text;

    /**
     * @param EntityManager $entityManager
     * @param EntityFormMapper $mapper
     */
    public function __construct(EntityManager $entityManager, EntityFormMapper $mapper)
    {
        $this->entityManager = $entityManager;
        $this->mapper = $mapper;
    }


    /**
     * @param Text $Text
     * @return Form
     */
    public function create(Text $Text = null)
    {
        $this->Text = $Text;

        $form = parent::create();

        $form->addTextArea('about', 'Co dělám', NULL, 15)
            ->setAttribute('id', 'wysiwyg');

        $form->addTextArea('contact', 'Kontakty', NULL, 15)
            ->setAttribute('id', 'wysiwyg2');

        $form->addText('tel', 'Zadejte telefon:')
            ->setRequired('Nezadali jste telefon!');

        $form->addText('email', 'Zadejte email:')
            ->setRequired('Nezadali jste email!');

        $form->addText('idNumber', 'Zadejte IČ:')
            ->setRequired('Nezadali jste IČ!');

        $form->addText('street', 'Zajdete ulici')
            ->setRequired('Nezadali jste ulici!');

        $form->addText('town', 'Zadejte město:')
            ->setRequired('Nezadali jste město!');

        $form->addText('firstMenu', 'První položka menu:')
            ->setRequired('Nezadali jste položku menu!');

        $form->addText('secondMenu', 'Druhá položka menu:')
            ->setRequired('Nezadali jste položku menu!');

        $form->addText('thirdMenu', 'Třetí položka menu:')
            ->setRequired('Nezadali jste položku menu!');

        $form->addText('fourthMenu', 'Čtvrtá položka menu:')
            ->setRequired('Nezadali jste položku menu!');

        $form->addText('fifthMenu', 'Pátá položka menu:')
            ->setRequired('Nezadali jste položku menu!');

        $form->addTextArea('footerText', 'Patička', NULL, 15)
            ->setAttribute('id', 'wysiwyg3');

        $form->addCheckbox('design', 'Změnit design');

        $form->addSubmit('send', $this->Text ? 'Upravit' : 'Přidat');
        $form->onSuccess[] = $this->process;

        if ($Text)
            $this->mapper->load($Text, $form);

        return $form;
    }

    /**
     * @param Form $form
     */
    public function process(Form $form)
    {
        $values = $form->getValues();


        if (!$this->Text) {

        } else {
            $this->Text->setAbout($values->about);
            $this->Text->setContact($values->contact);
            $this->Text->setTel($values->tel);
            $this->Text->setEmail($values->email);
            $this->Text->setIdNumber($values->idNumber);
            $this->Text->setStreet($values->street);
            $this->Text->setTown($values->town);
            $this->Text->setFirstMenu($values->firstMenu);
            $this->Text->setSecondMenu($values->secondMenu);
            $this->Text->setThirdMenu($values->thirdMenu);
            $this->Text->setFourthMenu($values->fourthMenu);
            $this->Text->setFifthMenu($values->fifthMenu);
            $this->Text->setFooterText($values->footerText);
            $this->Text->setDesign($values->design);

            $this->entityManager->persist($this->Text);
            $this->entityManager->flush($this->Text);
        }
    }
}