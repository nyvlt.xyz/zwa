<?php

namespace App\Forms;

use Nette\Application\UI\Form;
use Nette;

abstract class AdminForm extends Nette\Object
{

    /**
     * return Form
     */
    public function create() {
        $form = new Form();
        return $form;
    }
}