<?php
namespace App;

use Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route;

class RouterFactory
{
	/**
	 * @return \Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList();

		$router[] = new Route('admin/<presenter>/<action>/<id>', array(
			'module' => 'Admin',
			'presenter' => 'Dashboard',
			'action' => 'default',
			'id' => NULL,
		));

		$router[] = new Route('<presenter>[/<action>[/<id>]]', [
			'module' => 'Front',
			'presenter' => [
				Route::VALUE => 'Homepage',
				Route::FILTER_TABLE => [
					'o-mne' => 'Homepage',
					'kontakt' => 'Contact',
					'reference' => 'Articles',
					'publikace' => 'Publications'
				]
			],
			'action' => [
				Route::VALUE => 'default'
			],
		]);

		return $router;

	}
}