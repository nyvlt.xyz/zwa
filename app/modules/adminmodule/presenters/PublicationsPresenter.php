<?php
/**
 * Created by PhpStorm.
 * User: Tomáš Nývlt
 * Date: 22.09.15
 * Time: 10:57
 */

namespace App\AdminModule\Presenters;


use App\Model\Entity;
use Nette,
    App\Model;


/**
 * Publications presenter.
 */
class PublicationsPresenter extends BasePresenter
{
    /**
     * @inject
     * @var \Kdyby\Doctrine\EntityManager
     */
    public $EntityManager;

    /**
     * @var Model\PublicationManager @inject
     */
    public $publicationManager;

    /**
     * @var \App\Forms\PublicationForm @inject
     */
    public $publicationForm;

    /**
     * @var Entity\Publication
     */
    public $publication;


    public function startup()
    {
        parent::startup();
    }


    public function renderDefault()
    {
        $dao = $this->EntityManager->getRepository(Entity\Publication::getClassName());
        $this->template->publications = $dao->findAll();
    }


    /**
     * @return Nette\Application\UI\Form
     */
    public function createComponentPublicationForm()
    {
        $form = $this->publicationForm->create($this->publication);
        $form->onSuccess[] = function() {
            $this->flashMessage('Publikace byl úspěšně vložen.');
            $this->redirect('default');
        };
        return $form;
    }

    /**
     * @param $id
     */
    public function actionEdit($id)
    {
        $this->publication = $this->publicationManager->getPublication($id);
    }


    /**
     * @param $id
     */
    public function handleDelete($id)
    {
        $publication = $this->publicationManager->getPublication($id);
        if ($publication) {
            $this->publicationManager->delete($publication);
            $this->flashMessage('Publikace byla odebrána.');
        }
        $this->redirect('this');
    }
}
