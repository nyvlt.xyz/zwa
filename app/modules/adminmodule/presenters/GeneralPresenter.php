<?php
/**
 * Created by PhpStorm.
 * User: Tomáš Nývlt
 * Date: 22.09.15
 * Time: 10:57
 */

namespace App\AdminModule\Presenters;


use App\Model\Entity;
use Nette,
    App\Model;


/**
 * General presenter.
 */
class GeneralPresenter extends BasePresenter
{
    /**
     * @inject
     * @var \Kdyby\Doctrine\EntityManager
     */
    public $EntityManager;

    /**
     * @var Model\PageManager @inject
     */
    public $pageManager;

    /**
     * @var \App\Forms\PageForm @inject
     */
    public $pageForm;

    /**
     * @var Entity\Text
     */
    public $text;


    public function startup()
    {
        parent::startup();
    }


    public function renderDefault()
    {
        $dao = $this->EntityManager->getRepository(Entity\Text::getClassName());
        $this->template->texts = $dao->findAll();
    }


    /**
     * @return Nette\Application\UI\Form
     */
    public function createComponentPageForm()
    {
        $form = $this->pageForm->create($this->text);
        $form->onSuccess[] = function() {
            $this->flashMessage('Obecné texty byly úspěšně vloženy.');
            $this->redirect('Dashboard:default');
        };
        return $form;
    }

    /**
     * @param $id
     */
    public function actionEdit($id)
    {
        $this->text = $this->pageManager->getText($id);
    }
}
