<?php
/**
 * Created by PhpStorm.
 * User: Tomáš Nývlt
 * Date: 22.09.15
 * Time: 10:57
 */

namespace App\AdminModule\Presenters;


use App\Model\Entity;
use Nette,
    App\Model;


/**
 * Profile presenter.
 */
class ProfilePresenter extends BasePresenter
{
    /**
     * @inject
     * @var \Kdyby\Doctrine\EntityManager
     */
    public $EntityManager;

    /**
     * @var Model\ProfileManager @inject
     */
    public $profileManager;

    /**
     * @var \App\Forms\ProfileForm @inject
     */
    public $profileForm;

    /**
     * @var Entity\Profile
     */
    public $profile;


    public function startup()
    {
        parent::startup();
    }


    public function renderDefault()
    {
        $dao = $this->EntityManager->getRepository(Entity\Profile::getClassName());
        $this->template->profile = $dao->findAll();
    }


    /**
     * @return Nette\Application\UI\Form
     */
    public function createComponentProfileForm()
    {
        $form = $this->profileForm->create($this->profile);
        $form->onSuccess[] = function() {
            $this->flashMessage('Profesní profil byl úspěšně změněn.');
            $this->redirect('default');
        };
        return $form;
    }

    /**
     * @param $id
     */
    public function actionEdit($id)
    {
        $this->profile = $this->profileManager->getProfile($id);
    }
}
