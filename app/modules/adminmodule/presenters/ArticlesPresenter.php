<?php

namespace App\AdminModule\Presenters;


use App\Model\Entity;
use Nette,
    App\Model;


/**
 * Articles presenter.
 */
class ArticlesPresenter extends BasePresenter
{
    /**
     * @inject
     * @var \Kdyby\Doctrine\EntityManager
     */
    public $EntityManager;

    /**
     * @var Model\ArticleManager @inject
     */
    public $articleManager;

    /**
     * @var \App\Forms\ArticleForm @inject
     */
    public $articleForm;

    /**
     * @var Entity\Article
     */
    public $article;


    public function startup()
    {
        parent::startup();
    }


    public function renderDefault()
    {
        $dao = $this->EntityManager->getRepository(Entity\Article::getClassName());
        $this->template->articles = $dao->findAll();
    }


    /**
     * @return Nette\Application\UI\Form
     */
    public function createComponentArticleForm()
    {
        $form = $this->articleForm->create($this->article);
        $form->onSuccess[] = function() {
            $this->flashMessage('Článek byl úspěšně vložen.');
            $this->redirect('default');
        };
        return $form;
    }

    /**
     * @param $id
     */
    public function actionEdit($id)
    {
        $this->article = $this->articleManager->getArticle($id);
    }


    /**
     * @param $id
     */
    public function handleDelete($id)
    {
        $article = $this->articleManager->getArticle($id);
        if ($article) {
            $this->articleManager->delete($article);
            $this->flashMessage('Článek byl odebrán.');
        }
        $this->redirect('this');
    }
}
