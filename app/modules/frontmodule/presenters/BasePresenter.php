<?php

namespace App\FrontModule\Presenters;

use Nette,
	App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	/**
	 * @var Model\ArticleManager @inject
	 */
	public $articleManager;

	/**
	 * @var Model\PageManager @inject
	 */
	public $pageManager;

	public function beforeRender()
	{
		$this->template->articles = $this->articleManager->getArticlesActive();
		$this->template->page = $this->pageManager->getText(1);
		$this->template->anyVariable = 'any value';
	}
}
