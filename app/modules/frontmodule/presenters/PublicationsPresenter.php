<?php

namespace App\FrontModule\Presenters;

use Nette,
    App\Model;


/**
 * Publications presenter.
 */
class PublicationsPresenter extends BasePresenter
{

    /**
     * @var Model\PublicationManager @inject
     */
    public $publicationManager;

    public function startup()
    {
        parent::startup();
    }


    public function renderDefault()
    {
        $this->template->publications = $this->publicationManager->getPublications();
    }
}
