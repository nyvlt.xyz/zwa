<?php

namespace App\FrontModule\Presenters;

use Nette,
    App\Model;


/**
 * Articles presenter.
 */
class ArticlesPresenter extends BasePresenter
{

    /**
     * @var Model\ArticleManager @inject
     */
    public $articleManager;


    public function startup()
    {
        parent::startup();
    }


    public function renderDefault()
    {
        $this->template->articles = $this->articleManager->getArticles();
    }

    public function actionDetail($id)
    {
        $this->template->article = $this->articleManager->getArticle($id);
    }

}
