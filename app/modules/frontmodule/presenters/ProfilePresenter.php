<?php

namespace App\FrontModule\Presenters;

use Nette,
    App\Model;


/**
 * Publications presenter.
 */
class ProfilePresenter extends BasePresenter
{

    /**
     * @var Model\ProfileManager @inject
     */
    public $profileManager;

    public function startup()
    {
        parent::startup();
    }


    public function renderDefault()
    {
        $this->template->profile = $this->profileManager->getProfile(1);
    }
}