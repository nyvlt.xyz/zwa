<?php

namespace App\FrontModule\Presenters;

use App\Model;
use Nette;
use Nette\Application\UI\Form;
use stdClass;

/**
 * Sign in/out presenters.
 */
class SignPresenter extends BasePresenter
{
    /**
     * Sign-in form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSignInForm()
    {
        $form = new Nette\Application\UI\Form;
        $form->addText('username', 'E-mail')
            ->setRequired('Zadejte uživatelské jméno.');

        $form->addPassword('password', 'Heslo')
            ->setRequired('Zadejte heslo!');

        $form->addCheckbox('remember', 'Zapamatovat si mě');

        $form->addSubmit('send', 'Přihlásit se');

        // call method signInFormSucceeded() on success
        $form->onSuccess[] = array($this, 'signInFormSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param stdClass $values
     */
    public function signInFormSucceeded(Form $form, stdClass $values)
    {
        if ($values->remember) {
            $this->getUser()->setExpiration('14 days', FALSE);
        } else {
            $this->getUser()->setExpiration('20 minutes', TRUE);
        }

        try {
            $this->getUser()->login($values->username, $values->password);
            $this->redirect(':Admin:Dashboard:');

        } catch (Nette\Security\AuthenticationException $e) {
            $form->addError($e->getMessage());
        }
    }

    public function actionOut()
    {
        $this->getUser()->logout();
        $this->flashMessage('Byl jste odhlášen.');
        $this->redirect('in');
    }
}